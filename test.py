import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv('.env')

# Retrieve environment variables
db_server = os.getenv('DB_SERVER')
db_username = os.getenv('DB_USERNAME')
db_password = os.getenv('DB_PASSWORD')
db_name = os.getenv('DB_NAME')
db_port = os.getenv('DB_PORT')

# Print database username and password
print(db_username, db_password)

